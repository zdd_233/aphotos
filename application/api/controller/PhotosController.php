<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/8/4
 * Time: 21:49
 */

namespace app\api\controller;

use app\common\model\MemberImg;
use think\exception\ErrorException;
use think\Request;

class PhotosController extends ApiBaseController {

    /**
     * php异步请求
     * @param $host string 主机地址
     * @param $path string 路径
     * @param $param array 请求参数
     * @return string
     */
    private static function asyncRequest($host, $path, $param = array()){

       try{
            $query = isset($param) ? http_build_query($param) : '';
            $port = 8020;
            $errno = 0;
            $errstr = '';
            $timeout = 30; //连接超时时间（S）

            $fp = @fsockopen($host, $port, $errno, $errstr, $timeout);
            //$fp = stream_socket_client("tcp://".$host.":".$port, $errno, $errstr, $timeout);

            if (!$fp) {

                return '连接失败';
            }
            if ($errno || !$fp) {

                return $errstr;
            }

            stream_set_blocking($fp,0); //非阻塞
            stream_set_timeout($fp, 1);//响应超时时间（S）
            $out  = "POST " . $path . " HTTP/1.1\r\n";
            $out .= "host:" . $host . "\r\n";
            $out .= "content-length:" . strlen($query) . "\r\n";
            $out .= "content-type:application/x-www-form-urlencoded\r\n";
            $out .= "connection:close\r\n\r\n";
            $out .= $query;

            $result = @fputs($fp, $out);

            @fclose($fp);
            return $result;
        }catch (ErrorException $exception){
           return $exception;
        }

    }

    public function crease(Request $request){
        $id = input('id/d', 0);
        $img = MemberImg::where('id', $id)->find();
        if (empty($img)){
            return api_error('图片不存在', [], -1);
        }
        $save_name = $img['save_name'];
//        $save_name = $request->get('save_name', '');
        $explode = explode('/', $save_name);
        $dir = '/www/wwwroot/photos/public/uploads/'.$explode[0].DS.$explode[1];
        $py_exe = 'python';
        $host = '/www/wwwroot/clearMark/';
        $input_folder = "--input_folder $dir";     // 输入文件夹
        $output_folder = '--output_folder /www/wwwroot/photos/public/images';           // 输出文件夹
        $py_file = $host.'run.py';
//        $work_dir = 'D:/photos/clearMark';          // py工作目录
        $with_scratch = '--with_scratch';     // 折痕
        $gpu = '--GPU -1';

        // 调用py
        header("content-type:text/html;charset=utf-8");
        $order="$py_exe $py_file $gpu $with_scratch $input_folder $output_folder";
        // dump($order);
        exec($order, $out, $return_status);

        $img_name = explode('.',$explode[2]);
        $data = [
            'res_url' => 'http://'.$_SERVER['HTTP_HOST'].DS.'images'.DS.'final_output'.DS.$img_name[0].'.png',        //
        ];
        if ($return_status == 0){
            return api_success($data, '操作成功', 1);
        }
        return api_error('操作失败', [], -1);
    }

    /**
     * 模糊修复
     */
    public function vague(){
        try{
            $id = input('id/d', 0);
            $img = MemberImg::where('id', $id)->find();
            if (empty($img)){

            }
            $save_name = $img['save_name'];

//        $save_name = $request->get('save_name', '');
            $explode = explode('/', $save_name);
            $dir = '/www/wwwroot/photos/public/uploads/'.$explode[0].DS.$explode[1];
            $py_exe = 'python';
            $host = '/www/wwwroot/clearMark/';
            $input_folder = "--input_folder $dir";     // 输入文件夹
            $output_folder = '--output_folder /www/wwwroot/photos/public/images';           // 输出文件夹
            $py_file = $host.'run.py';
//        $work_dir = 'D:/photos/clearMark';          // py工作目录
//        $with_scratch = '--with_scratch';     // 折痕
            $gpu = '--GPU -1';
            // 调用py
            header("content-type:text/html;charset=utf-8");
            $order="$py_exe $py_file $gpu $input_folder $output_folder";
            exec($order, $out, $return_status);

            $img_name = explode('.',$explode[2]);
            $data = [
                'res_url' => 'http://'.$_SERVER['HTTP_HOST'].DS.'images'.DS.'final_output'.DS.$img_name[0].'.png',        //
            ];
            if ($return_status == 0){
//            return api_success($data, '操作成功', 1);
                return json([
                    'data' => $data,
                    'msg' => '操作成功',
                    'code' => 1
                ]);
            }
            return api_error('操作失败', [], -1);
        }catch (ErrorException $exception){
            return $exception;
        }

    }

    /**
     * 上色
     */
    public function coloring(){
        $id = input('id/d', 0);
        $img = MemberImg::where('id', $id)->find();
        if (empty($img)){
            return api_error('图片不存在', [], -1);
        }
        $save_name = $img['save_name'];

//        $save_name = $request->get('save_name', '');
        $explode = explode('/', $save_name);
//        $dir = '/www/wwwroot/photos/public/uploads/'.$explode[0].DS.$explode[1];
        $dir = '/www/wwwroot/photos/public/uploads/'.$save_name;
        $py_exe = 'python';
        $host = '/www/wwwroot/coloring/';
        $img_path = '-i '.$dir;
        $img_name = '-name '.explode('.',$explode[2])[0];
        $py_file = $host.'demo_release.py';
        // 调用py
        header("content-type:text/html;charset=utf-8");
        $order="$py_exe $py_file $img_path $img_name";
//        dump($order);
        exec($order, $out, $return_status);

        $img_name = explode('.',$explode[2]);
        $data = [
            'res_url' => 'http://'.$_SERVER['HTTP_HOST'].DS.'images'.DS.'coloring_output'.DS.$img_name[0].'_1.png',        //
            'res_url_sc' => 'http://'.$_SERVER['HTTP_HOST'].DS.'images'.DS.'coloring_output'.DS.$img_name[0].'_2.png',        //
        ];
        if ($return_status == 0){
            return api_success($data, '操作成功', 1);
        }
        return api_error('操作失败', [], -1);
    }
    
    /**
     * 上传图片
     */
    public function uploadImg(){
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file('image');
        $uuid = date('YmdHis').mt_rand(10000,99999);
        $img_name = date('YmdHis').mt_rand(10000,99999);
        $type = input('type', 1);

        // 移动到框架应用根目录/public/uploads/ 目录下
        if($file){
            $path = $_SERVER['DOCUMENT_ROOT'] . DS . 'uploads';
            $info = $file->move($path,date('Ymd').DS.$uuid.DS.$img_name);
            if($info){
                // 成功上传后 获取上传信息
                // 输出 jpg
//                echo $info->getExtension();
                // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
//                echo $info->getSaveName();
                // 输出 42a79759f284b767dfcb2a0197904287.jpg
//                echo $info->getFilename();
                // dump($info);
                $url = 'http://'.$_SERVER['HTTP_HOST'].DS.'uploads'.DS.$info->getSaveName();
                
                 // 创建图片信息
                $cdata = [
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'save_name' => $info->getSaveName(),
                    'path' => $path,
                    'type' => $type,
                    'url' => $url,
                ];
                
                $img = MemberImg::create($cdata);
                $id = $img->id;
                $host = "39.99.199.15";
                $param['id'] = $id;
                switch ($type){
                    case 1:
                        $path = "/vague";
                        self::asyncRequest($host,$path,$param);
                        break;
                    case 2:
                        $path = '/crease';
                        self::asyncRequest($host,$path,$param);
                        break;
                    case 3:
                        $path = '/coloring';
                        self::asyncRequest($host,$path,$param);
                        break;
                }

                $data = [
                    'id' => $id
                ];
//                return api_success($data, '操作成功', 1);
                return json([
                    'data' => $data,
                    'msg' => '操作成功',
                    'code' => 1
                ]);

            }else{
                // 上传失败获取错误信息
                echo $file->getError();
            }
        }
    }
    
    /**
     * 处理结果跟踪
     */
    public function track(){
        $id = input('id', 0);
        $uid = input('uid', 0);

        $img = MemberImg::where(['id' => $id])->find();

        if (empty($img)){
            return api_error('找不到图片信息', [], -1);
        }

        // 查询更新状态
        if ($img['status'] == 0){
            $img_info = explode('/', $img['save_name']);
            $img_name_info = explode('.', $img_info[2]);
            if ($img['type'] == 1 || $img['type'] == 2){
                $dir = '/www/wwwroot/photos/public/images/final_output';
                $files_arr = $this->searchFile($dir);
                foreach ($files_arr as $k => $v){
                    $file_info = explode('.', $v);
                    if ($img_name_info[0] == $file_info[0]){
                        // 文件夹中有当前名称的文件
                        $update_data = [
                            'status' => 1,
                            'res_url' => 'http://'.$_SERVER['HTTP_HOST'].DS.'images'.DS.'final_output'.DS.$v,
                            'update_time' => time()
                        ];
                        MemberImg::where(['id' => $id])->update($update_data);
                        break;
                    }
                }
            }
            if ($img['type'] == 3){
                $dir = '/www/wwwroot/photos/public/images/coloring_output';
                $files_arr = $this->searchFile($dir);
                foreach ($files_arr as $k => $v){
                    $file_info = explode('.', $v);
                    if ($img_name_info[0].'_1' == $file_info[0]){
                        // 文件夹中有当前名称的文件
                        $update_data = [
                            'status' => 1,
                            'res_url' => 'http://'.$_SERVER['HTTP_HOST'].DS.'images'.DS.'coloring_output'.DS.$v,
                            'update_time' => time()
                        ];
                        MemberImg::where(['id' => $id])->update($update_data);
                        $img = MemberImg::where(['id' => $id])->find();
                        break;
                    }
                }
            }
        }

        $return_data = [
            'status' => $img['status'],     // 0未处理 1处理完成
            'type' => $img['type'],         // 类型 1模糊 2折痕 3上色
            'res_url' => $img['res_url']    // 结果链接
        ];

        return api_success($return_data, '图片信息', 1);
    }
    
    public function searchFile($dir){
        // 获取某目录下所有文件、目录名（不包括子目录下文件、目录名）
        $handler = opendir($dir);
        while (($filename = readdir($handler)) !== false)
        {
            // 务必使用!==，防止目录下出现类似文件名“0”等情况
            if ($filename !== "." && $filename !== "..")
            {
                $files[] = $filename ;
            }
        }
        closedir($handler);
        
        return $files;
    }

    public function test(){
        dump($_SERVER['DOCUMENT_ROOT']);
    }
    
}