<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/8/15
 * Time: 16:21
 */

namespace app\common\model;

use think\model\concern\SoftDelete;

class MemberImg extends Model
{
    use SoftDelete;
    public $softDelete = true;
    protected $name = 'member_img';
    protected $autoWriteTimestamp = true;

}
