<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------


//use think\facade\Route;

Route::any('test','api/photos/test');
Route::any('crease','api/photos/crease');
Route::any('vague','api/photos/vague');
Route::any('coloring','api/photos/coloring');
Route::any('uploadImg','api/photos/uploadImg');
Route::any('track','api/photos/track');
Route::any('searchFile','api/photos/searchFile');

return [

];
